# succulent-container-setup

## Docker

Execute the following command to build the container:

```bash
docker build -t succulent-container-setup .
```

Execute the following command to run the container:

```bash
docker run -p 8080:8080 succulent-container-setup
```

### Worker threads

Depending on the number of threads the container has access to you can also set up `GUNICORN_WORKERS` environment variable. The suggested value is `2` to `4` per processor core.
